﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _7war.Models.DBTables
{
    public class MediaVoter
    {
        [Key]
        public int ID { get; set; }
        public int Fk_Media { get; set; }
        public int Fk_Voter { get; set; }
        public bool Vote { get; set; }
        public DateTime VoteDate { get; set; }
    }
}