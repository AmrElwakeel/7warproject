﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _7war.Models.DBTables
{
    public class Media
    {
        [Key]
        public int ID { get; set; }
        public string MediaName { get; set; }
        public string MediaUrl { get; set; }
        public int Fk_Competitor { get; set; }
        public int Fk_Competition { get; set; }
    }
}