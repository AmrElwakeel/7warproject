﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _7war.Models.DBTables
{
    public class Competitor
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Emai { get; set; }
        public string Phone { get; set; }
        public string PassWord { get; set; }
    }
}