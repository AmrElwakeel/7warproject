﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _7war.Models.DBTables
{
    public class Competition
    {
        [Key]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string File { get; set; }
        public DateTime AddedDate { get; set; }
    }
}