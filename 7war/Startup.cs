﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_7war.Startup))]
namespace _7war
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
